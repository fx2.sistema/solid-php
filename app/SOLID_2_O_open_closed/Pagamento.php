<?php

namespace App\SOLID_2_O_Open_Closed;

use App\SOLID_2_O_Open_Closed\Entities\Pix;
use App\SOLID_2_O_Open_Closed\interfaces\MetodoPagamento;

class Pagamento
{
    public function __construct(MetodoPagamento $metodoPagamento)
    {
        $this->metodoPagamento = $metodoPagamento;
    }

    public function comprar()
    {
        return $this->metodoPagamento->pagar();
    }
}