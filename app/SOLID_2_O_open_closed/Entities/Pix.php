<?php

namespace App\SOLID_2_O_Open_Closed\Entities;

use App\SOLID_2_O_Open_Closed\interfaces\MetodoPagamento;

class Pix implements MetodoPagamento
{
    public function pagar()
    {
        // logica monstro de pix
        return 'pago com pix';
    }
}