<?php

namespace App\SOLID_2_O_Open_Closed\Entities;

use App\SOLID_2_O_Open_Closed\interfaces\MetodoPagamento;

class Credito implements MetodoPagamento
{

    public function pagar()
    {
        // TODO: Implement pagar() method.
        return 'pago com credito';
    }
}