<?php

namespace App\SOLID_2_O_Open_Closed\interfaces;

interface MetodoPagamento
{
    public function pagar();
}