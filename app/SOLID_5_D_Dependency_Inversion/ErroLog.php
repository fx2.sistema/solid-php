<?php

namespace App\SOLID_1_S_single_responsability;

use App\SOLID_5_D_Dependency_Inversion\interfaces\Errors;

class ErroLog implements Errors
{
    public function logErro()
    {
        // salvo no BD o erro, dia, hora, motivo
    }
}