<?php

namespace App\SOLID_5_D_Dependency_Inversion\interfaces;

interface Errors
{
    public function logErro();
}