<?php

namespace App\SOLID_3_L_liskov_substitution;

class Veiculo
{
    public function __construct(Carro $carro)
    {
        $this->carro = $carro;
    }

    public function dirigindo()
    {
        return $this->carro->dirigir();
    }
}