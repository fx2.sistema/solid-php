<?php

namespace App\SOLID_3_L_liskov_substitution;

class Carro
{
    public function dirigir()
    {
        return 'carro acelerando';
    }
}