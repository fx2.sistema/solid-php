<?php

include_once '../../vendor/autoload.php';

//$carro = new \App\SOLID_3_L_liskov_substitution\Carro();
$monza = new \App\SOLID_3_L_liskov_substitution\Monza();
$veiculo = new \App\SOLID_3_L_liskov_substitution\Veiculo($monza);

print_r($veiculo->dirigindo());