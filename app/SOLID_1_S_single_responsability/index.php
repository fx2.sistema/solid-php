<?php

require_once '../../vendor/autoload.php';

$log = new \App\SOLID_1_S_single_responsability\ErroLog();
$product = new \App\SOLID_1_S_single_responsability\Product($log);

print_r($product->save([]));