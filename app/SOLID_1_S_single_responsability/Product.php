<?php

namespace App\SOLID_1_S_single_responsability;

class Product
{
    private ErroLog $erroLog;

    public function __construct(ErroLog $erroLog)
    {
        $this->erroLog = $erroLog;
    }

    public function save(array $request)
    {
        if (empty($request)) {
            $this->erroLog->logErro();
            throw new \Exception('Erro ao salvar, produto vazio');
        }

        // salvo no banco
        return 'Salvo com sucesso';
    }
}