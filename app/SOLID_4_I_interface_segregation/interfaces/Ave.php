<?php

namespace App\SOLID_4_I_interface_segregation\interfaces;

interface Ave
{
    public function voar();
    public function bicar();
}